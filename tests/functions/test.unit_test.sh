#!/usr/bin/env bash

# shellcheck source-path=SCRIPTDIR
FIXTURES_PATH="${ROOT_PATH}/tests/fixtures"

unit_test.test.assert_files_have_same_content_succeeds() {

  if test.assert_files_have_same_content "${FIXTURES_PATH}/file_01_with_content_A.txt" "${FIXTURES_PATH}/file_02_with_content_A.txt"; then
    return 0
  fi

  return 1
}
unit_test.test.assert_files_have_same_content_fails() {

  if ! test.assert_files_have_same_content "${FIXTURES_PATH}/file_01_with_content_A.txt" "${FIXTURES_PATH}/file_01_with_content_B.txt"; then
    return 0
  fi

  return 1
}

unit_test.test.assert_files_have_same_modification_time_succeeds() {

  FILE1="${DIR_TESTS_TMP}/test-assert_files_have_same_modification_time_succeeds.txt"
  touch "${FILE1}"

  if test.assert_files_have_same_modification_time "${FILE1}" "${FILE1}"; then
    return 0
  fi

  return 1
}

unit_test.test.assert_files_have_same_modification_time_fails() {

  FILE1="${DIR_TESTS_TMP}/test-assert_files_have_same_modification_time_fails-1.txt"
  touch "${FILE1}"
  sleep 1
  FILE2="${DIR_TESTS_TMP}/test-assert_files_have_same_modification_time_fails-2.txt"
  touch "${FILE2}"

  if ! test.assert_files_have_same_modification_time "${FILE1}" "${FILE2}"; then
    return 0
  fi

  return 1
}

unit_test.test.assert_file1_is_newer_than_file2_succeeds() {

  FILE1="${DIR_TESTS_TMP}/test-assert_file1_is_newer_than_file2_succeeds-1.txt"
  touch "${FILE1}"
  sleep 1
  FILE2="${DIR_TESTS_TMP}/test-assert_file1_is_newer_than_file2_succeeds-2.txt"
  touch "${FILE2}"

  if test.assert_file1_is_newer_than_file2 "${FILE2}" "${FILE1}"; then
    return 0
  fi

  return 1
}

unit_test.test.assert_file1_is_newer_than_file2_fails() {

  FILE1="${DIR_TESTS_TMP}/test-assert_file1_is_newer_than_file2_fails-1.txt"
  touch "${FILE1}"
  sleep 1
  FILE2="${DIR_TESTS_TMP}/test-assert_file1_is_newer_than_file2_fails-2.txt"
  touch "${FILE2}"

  if test.assert_file1_is_newer_than_file2 "${FILE1}" "${FILE2}"; then
    return 1
  fi

  return 0
}

unit_test.test.assert_file1_is_not_newer_than_file2_succeeds() {

  FILE1="${DIR_TESTS_TMP}/test-assert_file1_is_newer_than_file2_succeeds-1.txt"
  touch "${FILE1}"
  sleep 1
  FILE2="${DIR_TESTS_TMP}/test-assert_file1_is_newer_than_file2_succeeds-2.txt"
  touch "${FILE2}"

  if test.assert_file1_is_not_newer_than_file2 "${FILE1}" "${FILE2}"; then
    return 0
  fi

  return 1
}

unit_test.test.assert_file1_is_not_newer_than_file2_fails() {

  FILE1="${DIR_TESTS_TMP}/test-assert_file1_is_newer_than_file2_fails-1.txt"
  touch "${FILE1}"
  sleep 1
  FILE2="${DIR_TESTS_TMP}/test-assert_file1_is_newer_than_file2_fails-2.txt"
  touch "${FILE2}"

  if test.assert_file1_is_not_newer_than_file2 "${FILE2}" "${FILE1}"; then
    return 1
  fi

  return 0
}

unit_test.test.assert_file_exists_succeeds() {

  FILE1="${DIR_TESTS_TMP}/test-assert_file_exists_succeeds-1.txt"
  touch "${FILE1}"

  if test.assert_file_exists "${FILE1}"; then
    return 0
  fi

  return 1
}

unit_test.test.assert_file_exists_fails() {

  FILE1="${DIR_TESTS_TMP}/test-assert_file_exists_fails-1.txt"

  if test.assert_file_exists "${FILE1}"; then
    return 1
  fi

  return 0
}

unit_test.test.assert_dir_exists_succeeds() {

  DIR1="${DIR_TESTS_TMP}/test-assert_dir_exists_succeeds-1"
  mkdir -p "${DIR1}"

  if test.assert_dir_exists "${DIR1}"; then
    return 0
  fi

  return 1
}

unit_test.test.assert_dir_exists_fails() {

  DIR1="${DIR_TESTS_TMP}/test-assert_dir_exists_fails-1"

  if test.assert_dir_exists "${DIR1}"; then
    return 1
  fi

  return 0
}

unit_test.test.assert_dir_doesnt_exist_succeeds() {

  DIR1="${DIR_TESTS_TMP}/test-assert_dir_doesnt_exist_succeeds-1"

  if test.assert_dir_doesnt_exist "${DIR1}"; then
    return 0
  fi

  return 1
}

unit_test.test.assert_dir_doesnt_exist_fails() {

  DIR1="${DIR_TESTS_TMP}/test-assert_dir_doesnt_exist_fails-1"
  mkdir -p "${DIR1}"

  if test.assert_dir_doesnt_exist "${DIR1}"; then
    return 1
  fi

  return 0
}

unit_test.test.assert_string_ends_with_single_succeeds() {

  NEEDLE="CCC"
  HAYSTACK="AAA_BBB_CCC"

  if test.assert_string_ends_with_single "${NEEDLE}" "${HAYSTACK}"; then
    return 0
  fi

  return 1
}

unit_test.test.assert_string_ends_with_single_fails() {

  NEEDLE="CCC"
  HAYSTACK="AAA_BBB_CCCCCC"

  if test.assert_string_ends_with_single "${NEEDLE}" "${HAYSTACK}"; then
    return 1
  fi

  return 0
}

unit_test.test.assert_string_does_not_end_with_succeeds() {

  NEEDLE="AAA"
  HAYSTACK="AAA_BBB_CCC"

  if test.assert_string_does_not_end_with "${NEEDLE}" "${HAYSTACK}"; then
    return 0
  fi

  return 1
}

unit_test.test.assert_string_does_not_end_with_fails() {

  NEEDLE="CCC"
  HAYSTACK="AAA_BBB_CCC"

  if test.assert_string_does_not_end_with "${NEEDLE}" "${HAYSTACK}"; then
    return 1
  fi

  return 0
}
