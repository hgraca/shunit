# ShUnit

A unit test framework for shell scripts.

## How to use

You can install this library in your project, using [composer](https://getcomposer.org/).

Once you have composer installed in your system, you can run `composer install`,
which will install all dependencies and generate the autoloader in `vendor/autoload.sh`.

For an example, you can check the project [Bash Overlay](https://gitlab.com/hgraca/bash-overlay).

### Creating and running test suites

Create a test by naming it with the suffix `.unit_test.sh`, ie `my_test.unit_test.sh` and put your tests in it while
naming the functions with the prefix `unit_test.`

Ex:

Create this test file and put it in a folder `./tests`:

```bash
#!/usr/bin/env bash

unit_test.it_should_test_something() {

  if [ "666" == "666" ]; then
    return $0
  fi

  echo "Some error message explaining what failed"
  return $1
}
```

Execute this test, and all other tests in the folder and sub-folders, by running:
```shell
./vendor/bin/shunit ./tests
```

### Bootstrapping your tests

If you want to run some bootstrapping before running your tests, for example to set the logs level,
you can do so in the file `tests/bootstrap.sh`.

## Running this project tests

You can run this project test suite by running `composer test`.
