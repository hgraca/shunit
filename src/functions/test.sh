#!/usr/bin/env bash

test.cleanup_fs() {
  TEST_PATH=${1}

  rm -rf "${TEST_PATH}"
}

test.assert_files_have_same_content() {
  FILE_PATH_1="${1}"
  FILE_PATH_2="${2}"
  echo "test.assert_files_have_same_content ${FILE_PATH_1} ${FILE_PATH_2}"
  if file.have_same_content "${FILE_PATH_1}" "${FILE_PATH_2}"; then
    return 0
  fi

  stdout.log_error "The files '${FILE_PATH_1}' and '${FILE_PATH_2}' don't have the same content."
  return 1
}

test.assert_files_have_same_modification_time() {
  FILE_PATH_1="$1"
  FILE_PATH_2="$2"

  if file.have_same_modification_time "${FILE_PATH_1}" "${FILE_PATH_2}"; then
    return 0
  fi

  stdout.log_error "The files '${FILE_PATH_1}' and '${FILE_PATH_2}' don't have the same modification time: '$(file.get_modification_time "${FILE_PATH_1}")' vs '$(file.get_modification_time "${FILE_PATH_2}")'"
  return 1
}

test.assert_file1_is_newer_than_file2() {
  FILE_PATH_1="$1"
  FILE_PATH_2="$2"

  if file.file1_is_newer_than_file2 "${FILE_PATH_1}" "${FILE_PATH_2}"; then
    return 0
  fi

  stdout.log_error "The file '${FILE_PATH_1}' is not newer than '${FILE_PATH_2}': '$(file.get_modification_time "${FILE_PATH_1}")' vs '$(file.get_modification_time "${FILE_PATH_2}")'"
  return 1
}

test.assert_file1_is_not_newer_than_file2() {
  FILE_PATH_1="$1"
  FILE_PATH_2="$2"

  if file.file1_is_newer_than_file2 "${FILE_PATH_1}" "${FILE_PATH_2}"; then
    stdout.log_error "The file '${FILE_PATH_1}' is newer than '${FILE_PATH_2}': '$(file.get_modification_time "${FILE_PATH_1}")' vs '$(file.get_modification_time "${FILE_PATH_2}")'"
    return 1
  fi

  return 0
}

test.assert_file_exists() {
  FILE_PATH="$1"

  if file.exists "${FILE_PATH}"; then
    return 0
  fi

  stdout.log_error "The file '${FILE_PATH}' does not exist."
  return 1
}

test.assert_dir_exists() {
  DIR_PATH="$1"

  if dir.exists "${DIR_PATH}"; then
    return 0
  fi

  stdout.log_error "The directory '${DIR_PATH}' does not exist."
  return 1
}

test.assert_dir_doesnt_exist() {
  DIR_PATH="$1"

  if dir.exists "${DIR_PATH}"; then
    stdout.log_error "The directory '${DIR_PATH}' exists."
    return 1
  fi

  return 0
}

test.assert_string_ends_with_single() {
  NEEDLE="$1"
  HAYSTACK="$2"

  if ! string.ends_with "${NEEDLE}" "${HAYSTACK}" || string.ends_with "${NEEDLE}${NEEDLE}" "${HAYSTACK}"; then
    stdout.log_error "The given string '${HAYSTACK}' should end with a single '${NEEDLE}'."
    return 1
  fi

  return 0
}

test.assert_string_does_not_end_with() {
  NEEDLE="$1"
  HAYSTACK="$2"

  if string.ends_with "${NEEDLE}" "${HAYSTACK}"; then
    stdout.log_error "The given string '${HAYSTACK}' should not end with '${NEEDLE}'."
    return 1
  fi

  return 0
}
